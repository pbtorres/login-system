import React from 'react'
import * as C from './InputStyles';

function Input({ type, placeholder, value, onChange }) {
    return (
        <C.Input
            value={ value }
            onChange={ onChange }
            placeholder={ placeholder }
            type={ type }
        />
    )
}

export default Input;