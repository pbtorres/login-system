import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button/Button"
import useAuth from "../../hooks/useAuth"
import * as C from "./HomeStyles";

const Home = () => {
  const { signout } = useAuth();
  const navigate = useNavigate();

  return (
    <C.Container>
      <C.Title>Home</C.Title>
      <C.Description>
        Login System with React using Autentication, Routes, Context API and Hooks;
      </C.Description>
      <Button Text="Exit" onClick={() => [signout(), navigate("/")]}>
        Exit
      </Button>
    </C.Container>
  );
};

export default Home;