import React, { useState } from 'react';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import * as C from './SigninStyles';
import { Link, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';

function Signin() {
  const { signin } = useAuth();
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleLogin = () => {
    if(!email | !password) {
      setError("Fields can't empty!")
      return;
    }

    const res = signin(email, password);

    if(res) {
      setError(res);
      return;
    }

    navigate("/home");
  }

  return (
    <C.Container>
      <C.Label>LOGIN SYSTEM</C.Label>
      <C.Content>
        <Input
          type="email" 
          placeholder="Type your email..."
          value={ email }
          onChange={ (e) => [setEmail(e.target.value), setError("")]}
        />
        <Input 
          type="password"
          placeholder="Type your password..."
          value={ password }
          onChange={(e) => [setPassword(e.target.value), setError("")]}
        />
        <C.LabelError>{ error }</C.LabelError>

        <Button Text="Sign in" onClick={ handleLogin } />

        <C.LabelSignup>
          Don't have an account?
          <C.Strong>
            <Link to="/signup">&nbsp;Create account</Link>
          </C.Strong>
        </C.LabelSignup>
      </C.Content>
    </C.Container>
  )
}

export default Signin