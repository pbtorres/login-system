import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import { useState } from 'react';
import * as C from './SignupStyles';

const Signup = () => {
    const [email, setEmail] = useState("");
    const [emailConf, setEmailConf] = useState("");
    const [senha, setSenha] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();
  
    const { signup } = useAuth();
  
    const handleSignup = () => {
      if (!email | !emailConf | !senha) {
        setError("Fill in all fields");
        return;
      } else if (email !== emailConf) {
        setError("Emails are diferent");
        return;
      }
  
      const res = signup(email, senha);
  
      if (res) {
        setError(res);
        return;
      }
  
      alert("User successfully registered");
      navigate("/");
    };
  
    return (
      <C.Container>
        <C.Label>LOGIN SISTEM</C.Label>
        <C.Content>
          <Input
            type="email"
            placeholder="Type your email..."
            value={email}
            onChange={(e) => [setEmail(e.target.value), setError("")]}
          /> 
          <Input
            type="email"
            placeholder="Confirm your email..."
            value={emailConf}
            onChange={(e) => [setEmailConf(e.target.value), setError("")]}
          />
          <Input
            type="password"
            placeholder="Type password..."
            value={senha}
            onChange={(e) => [setSenha(e.target.value), setError("")]}
          />
          <C.LabelError>{error}</C.LabelError>
          
          <Button Text="Register" onClick={ handleSignup } />
         
          <C.LabelSignup>
            Already have an account?
            <C.Strong>
              <Link to="/">&nbsp;Signin</Link>
            </C.Strong>
          </C.LabelSignup>
        </C.Content>
      </C.Container>
    );
  };
  
  export default Signup;
