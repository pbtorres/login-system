import GlobalStyle from "./styles/global";
import RoutesApp from "./routes/RoutesApp";
import { AuthProvider } from "./contexts/AuthContext";

function App() {
  return (
    <>
      <AuthProvider>
        <RoutesApp />
        <GlobalStyle />
      </AuthProvider>
    </>
  );
}

export default App;
